from setuptools import setup

setup(
    name='PDIFinal',
    version='1.0',
    packages=['DetectaLibras'],
    install_requires=[
        'numpy',
        'opencv-python',
    ],
    url='https://gitlab.com/VictorGuerraVelosoUFV/PDI/PDILibras',
    license='BSD',
    author='victorgv',
    author_email='victorgvbh@gmail.com',
    description='Um detector de gestos de libras para webcam'
)
