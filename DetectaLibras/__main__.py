#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
from DetectaLibras.hu import binarizar_imagem, calcular_momentos_hu, comparar_momentos_hu
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Processa imagem da webcam e compara com imagens de gestos.')
    parser.add_argument('imgpath', metavar='IMG_PATH', type=str,
                        help='The path to the images folder')
    args = parser.parse_args()
    letraB = cv2.imread(args.imgpath + "Letra B.pgm")
    letraH = cv2.imread(args.imgpath + "Letra H.pgm")
    letraL = cv2.imread(args.imgpath + "Letra L.pgm")
    letraY = cv2.imread(args.imgpath + "Letra Y.pgm")

    m_h_l_B = calcular_momentos_hu(binarizar_imagem(letraB))
    m_h_l_H = calcular_momentos_hu(binarizar_imagem(letraH))
    m_h_l_L = calcular_momentos_hu(binarizar_imagem(letraL))
    m_h_l_Y = calcular_momentos_hu(binarizar_imagem(letraY))

    cap = cv2.VideoCapture(0)
    while cap.isOpened():
        # read image
        ret, frame = cap.read()

        # Retângulo para captura do gesto
        cv2.rectangle(frame, (250, 250), (50, 50), (0, 255, 0), 3)
        # Resize para ficar do mesmo tamanho que as imagens do banco de dados
        gesto = frame[50:250, 50:250]

        gesto = binarizar_imagem(gesto)
        gesto = cv2.bitwise_not(gesto)
        cv2.imshow("Gesto binarizado", gesto)
        gesto = cv2.resize(gesto, (128, 128))

        m_h = calcular_momentos_hu(gesto)
        comparar_momentos_hu(frame, m_h, (m_h_l_B, "Letra B"), (m_h_l_L, "Letra L"), (m_h_l_H, "Letra H"),
                             (m_h_l_Y, "Letra Y"))

        cv2.imshow("Frame", frame)
        # Apertar a letra ESQ para sair
        k = cv2.waitKey(10)
        if k == 27:
            break
