import operator
from functools import reduce
import numpy as np
import cv2


def binarizar_imagem(imagem):
    cinza = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
    ret, binarizada = cv2.threshold(cinza, 127, 255, cv2.THRESH_BINARY + cv2.THRESH_BINARY)
    return binarizada


def calcular_momentos_hu(binarizada):
    m_h = cv2.HuMoments(cv2.moments(binarizada)).flatten()
    m_h = -np.sign(m_h) * np.log10(np.abs(m_h))
    return m_h


def calcular_diferenca(momento_hu_base, momento_hu_alvo, nome):
    return reduce(operator.add, [abs(x - y) for x, y in zip(momento_hu_base, momento_hu_alvo)]), nome


def escreve_tela(frame, texto):
    cv2.putText(frame, texto, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 3, cv2.LINE_AA)


def comparar_momentos_hu(frame, m_h, *args):
    diferencas = [calcular_diferenca(m_h, arg, nome) for arg, nome in args]
    parecido = min(diferencas, key=lambda x: x[0])
    if parecido[0] < 10:
        escreve_tela(frame, parecido[1])
    else:
        escreve_tela(frame, "Letra nao reconhecida")
